<?php
    session_start();
    require_once "config.php";
    require_once "test_input.php";
    
    if (isset($_POST['register'])) {
        $secret = "6LdgpwMfAAAAAHS38T2ZSXmQjirPZP7crCUOD3iL";
        $response = $_POST['g-recaptcha-response'];
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip";
        $data = file_get_contents($url);
        $row = json_decode($data, true);

        if ($row['success'] == "true") {
            $name = test_input($_POST['name']);
            $username = test_input($_POST['username']);
            $password = md5(test_input($_POST['password']));
            $cpassword = md5(test_input($_POST['cpassword']));

            if ($password == $cpassword){
                $sql = "SELECT * FROM user WHERE user_username = '$username' AND user_password = '$password'";
                $result = mysqli_query($con, $sql);
                $rows = mysqli_num_rows($result);

                if ($rows == 0) {
                    $sql = "INSERT INTO `user`(`user_username`, `user_password`, `user_name`) VALUES ('$username','$password','$name')";
                    $result = mysqli_query($con, $sql);

                    if ($result) {
                        $sql = "SELECT * FROM user WHERE user_username = '$username' AND user_password = '$password'";
                        $result = mysqli_query($con, $sql);

                        if ($rows = mysqli_fetch_array($result)) {
                            $_SESSION['user_ID'] = $rows['user_ID'];
                            $name = $rows['user_name'];

                            echo "<script>alert('Welcome! ".$name."')</script>";
                            echo "<script>document.location.href='homepage.php';</script>";
                        }
                    }else {
                        echo "<script>alert('Sorry, your account could not be created. Please try again.')</script>";
                    }
                }else {
                    echo "<script>alert('You already have an account. We will redirect you to Login Page.')</script>";
                    echo "<script>document.location.href='login.php';</script>";
                }
            }else {
                echo "<script>alert('Please make sure your password and confirm password is same.')</script>";
            }
        }else {
            echo "<script>alert('Please verify yourself.')</script>";
        }
    }

    if (isset($_POST['login'])) {
        $secret = "6LdgpwMfAAAAAHS38T2ZSXmQjirPZP7crCUOD3iL";
        $response = $_POST['g-recaptcha-response'];
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip";
        $data = file_get_contents($url);
        $row = json_decode($data, true);

        if ($row['success'] == "true") {
            $username = test_input($_POST['username']);
            $password = md5(test_input($_POST['password']));
            
            $sql = "SELECT * FROM user WHERE user_username = '$username' AND user_password = '$password'";
            $result = mysqli_query($con, $sql);

            if ($rows = mysqli_fetch_array($result)) {
                $_SESSION['user_ID'] = $rows['user_ID'];
                $name = $rows['user_name'];
                
                echo "<script>alert('Welcome! ".$name."')</script>";
                echo "<script>document.location.href='homepage.php';</script>";
            }else {
                echo "<script>alert('Your username or password is incorrect. Please try again')</script>";
            }
        }else {
            echo "<script>alert('Please verify yourself.')</script>";
        }
    }
    
    if (isset($_POST['addWeight'])) {
        $user_ID = $_SESSION['user_ID'];
        $weight_target = test_input($_POST['weight_target']);
        $weight = test_input($_POST['weight']);
        $height = test_input($_POST['height']);
        $weight_date = test_input($_POST['weight_date']);

        $sql = "INSERT INTO `weight_record`(`user_ID`, `weight_date`, `height`, `weight`, `weight_target`) VALUES ('$user_ID','$weight_date','$height','$weight','$weight_target')";
        $result = mysqli_query($con, $sql);
        
        if ($result) {
            echo "<script>alert('Your record updated.')</script>";
            echo "<script>document.location.href='homepage.php';</script>";
        }else {
            echo "<script>alert('Your record not updated. Please try again.')</script>";
            echo "<script>document.location.href='addWeight.php';</script>";
        }
    }
    
    if (isset($_POST['editWeight'])) {
        $weight_ID = test_input($_POST['weight_ID']);
        $weight = test_input($_POST['weight']);
        $height = test_input($_POST['height']);
        $weight_date = test_input($_POST['weight_date']);

        $sql = "UPDATE `weight_record` SET `weight_date` = '$weight_date', `height` = '$height', `weight` = '$weight' WHERE `weight_record`.`weight_ID` = '$weight_ID'";
        $result = mysqli_query($con, $sql);

        if ($result) {
            echo "<script>alert('The record has been edited.')</script>";
            echo "<script>document.location.href='homepage.php';</script>";
        }else {
            echo "<script>alert('The record has not been edited. Please try again.')</script>";
            echo "<script>document.location.href='editWeight.php';</script>";
        }
    }
    
    if (isset($_GET['logout'])) {
        session_unset();
        session_destroy();
        echo "<script>document.location.href='login.php';</script>";
    }
?>