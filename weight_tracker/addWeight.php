<?php
    require_once "session.php";
    if(!isset($_SESSION['user_ID'])){
        header("Location: login.php");
        exit();
    }
?>
<html>
    <head>
        <title>Weight Tracker | Add Weight</title>
        <link rel="stylesheet" href="style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
    </head>
    <body>
        <div class="navbar">
            <ul>
                <li><a href="homepage.php">Home</a></li>
                <li><a href="addWeight.php" id="active">Add Weight</a></li>
                <li><a href='session.php? logout=logout'>Logout</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="card">
                <form action="" method="POST">
                    <fieldset>
                        <h2>Add Weight</h2>
                        <?php
                            $user_ID = $_SESSION['user_ID'];
                            $sql = "SELECT * FROM weight_record WHERE user_ID = '$user_ID' ORDER BY weight_date DESC";
                            $result = mysqli_query($con, $sql);
                            $rows = mysqli_num_rows($result);

                            if ($rows == 0) {

                                echo "
                                    <label>Weight Target (kg)</label><br>
                                    <input type=\"number\" name=\"weight_target\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" required><br><br>
                                    <label>Weight (kg)</label><br>
                                    <input type=\"number\" name=\"weight\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" required><br><br>
                                    <label>Height (cm)</label><br>
                                    <input type=\"number\" name=\"height\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" required><br><br>
                                    <label>Date (YYYY/mm/dd)</label><br>
                                    <input type=\"date\" name=\"weight_date\"><br><br>
                                ";
                            }else {
                                $rows = mysqli_fetch_assoc($result);
                                $weight_target = $rows['weight_target'];
                                $height = $rows['height'];
                                
                                echo "
                                    <label>Weight Target (kg)</label><br>
                                    <input type=\"number\" name=\"weight_target\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" value=\"$weight_target\" required><br><br>
                                    <label>Weight (kg)</label><br>
                                    <input type=\"number\" name=\"weight\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" required><br><br>
                                    <label>Height (cm)</label><br>
                                    <input type=\"number\" name=\"height\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" value=\"$height\" required><br><br>
                                    <label>Date (YYYY/mm/dd)</label><br>
                                    <input type=\"date\" name=\"weight_date\"><br><br>
                                ";
                            }
                        ?>
                        <input type="submit" name="addWeight" value="Submit">
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>