<?php
    require_once "session.php";
?>
<html>
    <head>
        <title>Weight Tracker | Login Page</title>
        <link rel="stylesheet" href="style2.css">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    </head>
    <body>
        <div class="container">
            <div class="card">
                <form action="" method="POST">
                    <h2>Login</h2>
                    <label>Username</label><br>
                    <input type="text" name="username" placeholder="Please enter your username" title="Please insert your username" autocomplete="off" required><br><br>
                    <label>Password</label><br>
                    <input type="password" name="password" placeholder="Please enter your password" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters" autocomplete="off" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required><br><br>
                    <div class="g-recaptcha" data-sitekey="6LdgpwMfAAAAACOpQRfHr5AouJmHxwDKehbeGR6a"></div><br>
                    <a href="register.php">Click here to register.</a><br><br>
                    <input type="submit" name="login" value="Login">
                </form>
            </div>
        </div>
    </body>
</html>