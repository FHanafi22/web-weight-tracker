<?php
    require_once "session.php";
    if(!isset($_SESSION['user_ID'])){
        header("Location: login.php");
        exit();
    }

    $weight_ID = $_GET['weight_ID'];

    $sql = "DELETE FROM weight_record WHERE weight_ID='$weight_ID'";
    $result = mysqli_query($con, $sql);

    if ($result) {
        echo "<script>alert('The record has been deleted.')</script>";
        echo "<script>document.location.href='homepage.php';</script>";
    }else {
        echo "<script>alert('The record has not been deleted. Please try again.')</script>";
        echo "<script>document.location.href='homepage.php';</script>";
    }
?>