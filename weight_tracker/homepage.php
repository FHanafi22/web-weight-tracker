<?php
    require_once "session.php";
    if(!isset($_SESSION['user_ID'])){
        header("Location: login.php");
        exit();
    }
?>
<html>
    <head>
        <title>Weight Tracker | Home</title>
        <link rel="stylesheet" href="style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
    </head>
    <body>
        <div class="navbar">
            <ul>
                <li><a href="homepage.php" id="active">Home</a></li>
                <li><a href="addWeight.php">Add Weight</a></li>
                <li><a href='session.php? logout=logout'>Logout</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="card">
                <table>
                    <tr>
                        <th>Num.</th>
                        <th>Date (YYYY/mm/dd)</th>
                        <th>Height (cm)</th>
                        <th>Current Weight (kg)</th>
                        <th>Target Weight (kg)</th>
                        <th>Weight To Lost (kg)</th>
                        <th>BMI (kg/m2)</th>
                        <th>Edit</th>
                    </tr>
                    <?php
                        require_once "config.php";

                        $user_ID = $_SESSION['user_ID'];
                        $counter = 1;

                        $sql = "SELECT * FROM weight_record WHERE user_ID = '$user_ID' ORDER BY weight_date DESC";
                        $result = mysqli_query($con, $sql);

                        while ($rows = mysqli_fetch_assoc($result)) {
                            $weight_ID = $rows['weight_ID'];
                            $weight_date = $rows['weight_date'];
                            $height = $rows['height'];
                            $heightNew = $height / 100;
                            $current_weight = $rows['weight'];
                            $weight_target = $rows['weight_target'];

                            $weight_loss = $current_weight - $weight_target;
                            $bmi = round($current_weight / ($heightNew * $heightNew), 2);

                            echo "
                            <tr>
                                <td>".$counter."</td>
                                <td>".$weight_date."</td>
                                <td>".$height."</td>
                                <td>".$current_weight."</td>
                                <td>".$weight_target."</td>
                                <td>".$weight_loss."</td>
                                <td>".$bmi."</td>
                                <td>
                                    <button type=\"button\" onclick=\"document.location='editWeight.php? weight_ID=$weight_ID'\">Edit</button><br>
                                    <button type=\"button\" onclick=\"document.location='deleteWeight.php? weight_ID=$weight_ID'\">Delete</button>
                                </td>
                            </tr>
                            ";
                            $counter++;
                        }
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>