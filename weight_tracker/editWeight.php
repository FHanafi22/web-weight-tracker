<?php
    require_once "session.php";
    if(!isset($_SESSION['user_ID'])){
        header("Location: login.php");
        exit();
    }
?>
<html>
    <head>
        <title>Weight Tracker | Edit Weight</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="navbar">
            <ul>
                <li><a href="homepage.php" id="active">Home</a></li>
                <li><a href="addWeight.php">Add Weight</a></li>
                <li><a href='session.php? logout=logout'>Logout</a></li>
            </ul>
        </div>
        <div class="container">
            <div class="card">
                <form action="" method="POST">
                    <fieldset>
                        <h2>Edit Weight</h2>
                        <?php
                            $weight_ID = $_GET['weight_ID'];
                            $sql = "SELECT * FROM weight_record WHERE weight_ID = '$weight_ID'";
                            $result = mysqli_query($con, $sql);
                        
                            if ($rows = mysqli_fetch_assoc($result)) {
                                $weight_date = $rows['weight_date'];
                                $height = $rows['height'];
                                $current_weight = $rows['weight'];
                                
                                echo "
                                    <label>Weight (kg)</label><br>
                                    <input type=\"number\" name=\"weight\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" value=\"$current_weight\" required><br><br>
                                    <label>Height (cm)</label><br>
                                    <input type=\"number\" name=\"height\" min=\"0.1\" max=\"200.0\" step=\"0.1\" placeholder=\"1.0\" value=\"$height\" required><br><br>
                                    <label>Date (YYYY/mm/dd)</label><br>
                                    <input type=\"date\" name=\"weight_date\" value=\"$weight_date\" required><br><br>   
                                    <input type=\"hidden\" name=\"weight_ID\" value=\"$weight_ID\" required>
                                    <input type=\"submit\" name=\"editWeight\" value=\"Edit\">
                                ";
                            }
                        ?>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>