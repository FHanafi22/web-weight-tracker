<?php
    require_once "session.php";
?>
<html>
    <head>
        <title>Weight Tracker | Register Page</title>
        <link rel="stylesheet" href="style2.css">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    </head>
    <body>
        <div class="container">
            <div class="card">
                <form action="" method="POST">
                    <h2>Register</h2>
                    <label>Name</label><br>
                    <input type="text" name="name" placeholder="Please enter your name" title="Please insert your name" autocomplete="off" required><br><br>
                    <label>Username</label><br>
                    <input type="text" name="username" placeholder="Please enter your username" title="Please insert your username" autocomplete="off" required><br><br>
                    <label>Password</label><br>
                    <input type="password" name="password" placeholder="Please enter your password" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters" autocomplete="off" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required><br><br>
                    <label>Confirm Password</label><br>
                    <input type="password" name="cpassword" placeholder="Please enter your password" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters" autocomplete="off" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" required><br><br>
                    <div class="g-recaptcha" data-sitekey="6LdgpwMfAAAAACOpQRfHr5AouJmHxwDKehbeGR6a"></div><br>
                    <a href="login.php">Already have an account?</a><br><br>
                    <input type="submit" name="register" value="Login">
                </form>
            </div>
        </div>
    </body>
</html>